# README #
Opportunity to learn

### Assessment task ###
We would like to ask you to complete a small technical challenge, and then have a follow-up discussion where you can
walk us through the code and demonstrate it working.

1) The task would be to write a simple **Go-based operator** using the **operator SDK**.

2) The **operator** should allow you to **install** and **uninstall** a basic **standalone instance** of **Redis** via a **CRD**.

When you are ready, let us know and we will schedule a Zoom call and you can walk us through the code and demonstrate the working operator.
I appreciate you haven’t yet had exposure to operators, CRDs or Go. We’re not expecting you to become an expert in all this for this task,
but it should be straightforward to get a simple operator working using the various guides available and talk us through the high-level concepts.

### To Do and Areas to cover ###

* Redis and its deployments
* GO language just covering the basics
* GO operator SDK
* Run environments eg docker and kubernetes
* How to run demo


### Prerequisites ###

* go version 1.16.7
* go operator-sdk 1.12
* docker and kubectl

### Completed ###

1) Installed GO and Go-operator SDK
2) Created go-operator project
    1) mkdir matrixx-go-redis; cd matrixx-go-redis
    2) export GO111MODULE=on
    3) operator-sdk init --domain charliebsimms.co.uk --repo bitbucket.org/charliebsimms/redis-op
3) Install kubernetes in AWS in 2 VM's master and single worker node
    1) Can ssh on to master and run kubectl, all nodes are running
    2) Need to figure out how to connect to master node from local machine using kubectl probably need to understand more about iptables
4) Use AWS EKS
    1) eksctl create cluster --name=matrixx --nodes=1 --region=eu-west-1 --node-type=m5.xlarge --auto-kubeconfig
    2) export KUBECONFIG=~/.kube/eksctl/clusters/matrixx
5) Check if you have sufficient privileges on kubernetes to deploy operator
    1) kubectl auth can-i create deployments
    2) kubectl auth can-i list secrets
6) Need to understand Manager, API, controllers and CRD (Custom Resource Definition)
    1) https://book.kubebuilder.io/cronjob-tutorial/basic-project.html
    2) https://sdk.operatorframework.io/docs/building-operators/golang/tutorial/
    3) https://www.ibm.com/cloud/blog/demystifying-operator-development
7) Golang based redis operator using https://operatorhub.io/operator/redis-operator
    1) Deployed OLM (Operator Lifecycle Manager) into k8s
    2) Deployed redis operator into k8s
8) Develop my redis operator using operator SDK
    1) Using the memcached-operator project as template
    2) Namespace: operators
    3) Create new API and controller
        1) operator-sdk create api --group cache --version v1alpha1 --kind Redis --resource --controller
        2) go get github.com/go-logr/logr@v0.4.0
           go get k8s.io/api/apps/v1@v0.21.2
           go get k8s.io/api/core/v1@v0.21.2
        3) Update apis myredis_types.go using as template: https://github.com/operator-framework/operator-sdk/blob/latest/testdata/go/v3/memcached-operator/api/v1alpha1/memcached_types.go
        4) Update myredis_controller.go using as template: https://github.com/operator-framework/operator-sdk/blob/latest/testdata/go/v3/memcached-operator/controllers/memcached_controller.go
    4) ran the following: make generate, make manifests, make install, make docker-build docker-push
    5) pushed image to docker.io/charliesimms/myredis-operator
        1) export IMG=docker.io/charliesimms/myredis-operator:LATEST
        2) export NAMESPACE=myredis-operator-system
        3) cd config/manager
        4) kustomize edit set image controller=${IMG}
        5) kustomize edit set namespace "${NAMESPACE}"
        6) cd config/default
        7) kustomize edit set namespace "${NAMESPACE}"
9) Deploy operator to k8s
    1) make deploy
    2) kubectl get nodes
    3) kubectl get deployments -n myredis-operator-system
    4) kubectl get pods -n myredis-operator-system
    5) kubectl describe pod cs-operator-redis-controller-manager-5c884c9d4-qrmxs -n myredis-operator-system
    6) kubectl logs cs-operator-redis-controller-manager-5c884c9d4-qrmxs -n myredis-operator-system -c kube-rbac-proxy
    7) kubectl logs cs-operator-redis-controller-manager-5c884c9d4-qrmxs -n myredis-operator-system -c manager
10) Kubernetes RBAC need better understanding
    1) kubectl get crd
    2) kubectl get clusterrolebindings
    4) kubectl get clusterrolebindings redis-operator-manager-rolebinding -o yaml
    5) kubectl get clusterrole redis-operator-manager-role -o yaml
11) Delete operator
    1) make undeploy
12) Delete using kubectl
    1) kubectl delete deployment,service -l control-plane=controller-manager -n myredis-operator-system
    2) kubectl delete role,rolebinding --all -n myredis-operator-system



## Kubernetes Control Loop Diagram
![cached image](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuIfABKXDBB7AJofEBIfBjUPIKB1AoKnBi-PAIapEBayiIh5AJ2wnvd98pKi1AWS0)
## Kubernetes Operator Diagram
![cached image](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuShBJqbLKCekJKejoaijIIrMICufBIv9BLAgXahb5wGM9PRa5oafL7Cbv-SMvEUbvfKeH3ldvvKKvUVaf1R5eWW7kSHorIA9-Flu0laEgNafGDi0)


### references ###

* https://golang.org/doc/install
* https://sdk.operatorframework.io/docs/overview/
* https://sdk.operatorframework.io/docs/building-operators/golang/tutorial/
* https://book.kubebuilder.io/cronjob-tutorial/basic-project.html
* https://kubernetes.io/docs/tutorials/configuration/configure-redis-using-configmap/
* https://hub.docker.com/_/redis
* https://redis.io/topics/rediscli
* https://ot-container-kit.github.io/redis-operator/guide/#supported-features
* https://operatorhub.io/operator/redis-operator
* https://markheath.net/post/exploring-redis-with-docker
* https://cloud.redhat.com/blog/introducing-the-operator-framework
* https://github.com/operator-framework/getting-started
* https://operatorhub.io/operator/redis-operator
* https://github.com/kube-incubator/redis-operator
* https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
* https://docs.projectcalico.org/getting-started/kubernetes/quickstart
* https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/configure-cgroup-driver/
* https://aws.amazon.com/blogs/containers/introducing-cdk-for-kubernetes/
* https://cdk8s.io/docs/latest/getting-started/
* https://www.digitalocean.com/community/tutorials/understanding-database-sharding
* https://developer.ibm.com/learningpaths/kubernetes-operators/develop-deploy-simple-operator/create-operator/

### Relevant courses ###
* https://www.whizlabs.com/learn/course/learn-kubernetes-with-aws-and-docker/265
* https://www.udemy.com/course/go-the-complete-developers-guide/
* https://learn.acloud.guru/handson/66797cef-5202-4ed6-bdc5-37349ef340bd